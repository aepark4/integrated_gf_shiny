# Code to produce Shiny app for - Integrating trawl and longline surveys across British Columbia improves groundfish distribution predictions

__Main author:__  Ashley Park  
__Contributors:__ Patrick Thompson, Robert Skelly  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: ashley.park@dfo-mpo.gc.ca


- [Objective](#objective)
- [Status](#status)
- [Contents](#contents)
- [Requirements](#requirements)
- [References](#references)


## Objective
Shiny app to display interactive figures as a supplement to the manuscript. It allows users to visualize the estimated response curves for each environmental gradient, spatially compare the different models, visualize the probability of occurrence and uncertainty in the final selected model, and visualize the coastwide species richness and groundfish assemblages for the 65 groundfish species included in the analysis. Users can select different groundfish species and environmental covariates to compare.

## Status
Complete

## Contents
R code and html widgets for running R Shiny app. 

## Requirements

### Software
* R version 4.0.2 or newer (https://www.r-project.org/)
* For full list of required R packages see app.R 

### Input Data
* For full list of required input data see app.R 
* plot 2-4 html widgets 

## References
Thompson P. L., S. C. Anderson, J. Nephin, C.K. Robb, B. Proudfoot, A.E. Park, D.R. Haggarty, and E. Rubidge. 2022. Integrating trawl and longline surveys across British Columbia improves groundfish distribution predictions. Canadian Journal of Fisheries and Aquatic Sciences. http://dx.doi.org/10.1139/cjfas-2022-0108

